using DMT;
using Harmony;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.IO;
using UnityEngine;

class Khaine_QuestJournal_Patch
{
    public class Khaine_QuestJournal_Logger
    {
        public static bool blDisplayLog = true;

        public static void Log(String strMessage)
        {
            if (blDisplayLog)
                UnityEngine.Debug.Log(strMessage);
        }
    }

    public class Khaine_QuestJournal_Init : IHarmony
    {
        public void Start()
        {
            Khaine_QuestJournal_Logger.Log(" Loading Patch: " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(QuestJournal))]
    [HarmonyPatch("GetCurrentFactionTier")]
    public class Khaine_QuestJournal_GetCurrentFactionTier_Patch
    {
        // Loops around the instructions and removes the return condition.
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            int counter = 0;

            // Grab all the instructions
            var codes = new List<CodeInstruction>(instructions);
            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_5) // Increasing the 5
                {
                    counter++;
                    Khaine_QuestJournal_Logger.Log("Adjusting 5 to 10"); 

                    codes[i].opcode = OpCodes.Ldc_I4; // convert to the right thing
                    codes[i].operand = 10;
                    Khaine_QuestJournal_Logger.Log("Done with 5 to 10");
                }
            }
            return codes.AsEnumerable();
        }
    }
}