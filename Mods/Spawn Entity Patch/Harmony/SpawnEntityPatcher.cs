//Harmony Patch: Patches in an area check to BlockSpawnEntiy
//Author: Mythix(dino)
using Harmony;
using System;
using System.Reflection;
using UnityEngine;
using DMT;

[HarmonyPatch(typeof(BlockSpawnEntity))]
[HarmonyPatch("UpdateTick")]
class test
{
    static bool Prefix(BlockSpawnEntity __instance, ref Vector3i _blockPos, ref WorldBase _world)
    {
        foreach (Entity entity in GameManager.Instance.World.Entities.list)
        {
            if (entity is EntityNPC && entity.IsAlive())
            {
                float checknearby = Math.Abs(Vector3.Distance(entity.position, _blockPos.ToVector3()));
                if (checknearby > 0 && checknearby < 5)
                {
                    _world.GetWBT().AddScheduledBlockUpdate(0, _blockPos, __instance.blockID, 320UL);
                    return false;
                }
            }
        }
        return true;
    }
}