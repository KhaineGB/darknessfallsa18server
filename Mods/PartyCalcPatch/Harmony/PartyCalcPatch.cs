using DMT;
using Harmony;
using UnityEngine;
using System;
using System.Collections.Generic;

class Khaine_PartyCalcPatch
{
        [HarmonyPatch(typeof(GameStageDefinition))]
        [HarmonyPatch("CalcPartyLevel")]
        public class MethodCalcPartyLevel
        {
            static void Postfix(ref List<int> playerGameStages, ref int __result)
            {
                int partycount = playerGameStages.Count;
                double GS = __result;
                //switch (partycount)
              
					if (partycount == 2)
					{
						GS *= 0.75;
					} else if (partycount > 2 && partycount < 6)
					{
						GS *= (partycount / 2);
					}
                
                __result = Convert.ToInt32(GS);
            }
        }
}