SplatFixer (by Damocles) , for Alpha 18
(for Alpha-18) converts any png to the correct splat3.png
pixels detected: red (255) -> asphalt, green (255) -> gravel

You can use this tool to "fix" older splat3.png images to the A18 format,
and also make it easier to edit and convert splat road maps.
The loaded image does not need an alpha channel.

You can save the image in debug mode to make it easier to see with an image display program.


commands and parameters
-in [filename] , (default is splat3.png) file read in, such as myroads.png or "my roads.png"
-out [filename] , (default is splat3_fixed.png) file saved to, such as roads_fixed.png or "my roads fixed.png"
-debug on  , when turned on, it will save the alpha channed 50% transparent to see the image better, DONT use those for the game

example parameters:
-in roads.png
-in splat3.png -out splat3.png
-in "C:\my hobby folder\megacity32k\roads.png"
-out splat3_fix.png
-in roadsEdit.png -out roadsFixed.png -debug on
(with no parameters it will by default load splat3.png and save to splat3_fixed.png)
